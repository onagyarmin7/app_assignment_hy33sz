#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "stb_image.h"
#include "stb_image_write.h"
#include <stdio.h>
#include <stdint.h>
#include <time.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define TILE_SIZE 16
#define CHANNELS 3

__global__ void Convert2Grayscale(uint8_t* dev_img, uint8_t* dev_result, int img_width, int img_height)
{
	int threads_needed = img_width * img_height; //Sz�ks�ges sz�lak sz�ma
	if (threads_needed < (blockDim.x * blockDim.y) * (blockIdx.x * blockIdx.y) + (threadIdx.x * threadIdx.y)) //A nem sz�ks�ges sz�lak le�ll�t�sa
	{
		return; 
	}

	__shared__ int shr_img[TILE_SIZE][TILE_SIZE][3]; //Shared mem�ri�t haszn�lunk a k�p elt�rol�s�hoz: IMG: W x H x CHANNELS
	//A -3 az�rt sz�ks�ges, mert 13x13x3 = 507 byte, ezt megszorozva az int t�pus m�ret�vel (4 byte): 507 x 4 = 2028 b�jt.
	//Az occupancy calculator szerint 256 sz�l / blokk �s 2048 byte shared memory eset�n a GPU kihaszn�lts�ga 100%-os.
	//(Regiszterm�ret: 32 / sz�l)

	int col = blockIdx.x * blockDim.x + threadIdx.x; // Oszlopindex
	int row = blockIdx.y * blockDim.y + threadIdx.y; // Sorindex

	int x = threadIdx.x;
	int y = threadIdx.y;

	int grayOffset = row * img_width + col; //T�mbindex lek�pez�se sor �s oszlop alapj�n.
	int rgbOffset = grayOffset * CHANNELS; //Index eltol�sa a h�rom csator�n�nak megfelel�en.

	//Shared memory inicializ�l�sa.

	shr_img[x][y][0] = dev_img[rgbOffset + 0]; //Shared memory x,y hely�nek felt�lt�se az adott pixel (x,y) r sz�n�rt�k�vel.
	shr_img[x][y][1] = dev_img[rgbOffset + 1]; //Shared memory x,y hely�nek felt�lt�se az adott pixel (x,y) r sz�n�rt�k�vel.
	shr_img[x][y][2] = dev_img[rgbOffset + 2]; //Shared memory x,y hely�nek felt�lt�se az adott pixel (x,y) r sz�n�rt�k�vel.

	unsigned char r = shr_img[x][y][0]; //R kinyer�se - Nem sz�ks�ges ezek v�ltoz�ba val� kiszervez�se, explicit beilleszthet� lenne a kifejez�s a k�pletbe is, de �gy �rthet�bb.
	unsigned char g = shr_img[x][y][1];
	unsigned char b = shr_img[x][y][2];

	dev_result[grayOffset] = 0.21f * r + 0.71f * g + 0.07f * b; //Az eredm�nypixel el��ll�t�sa, a h�rom �rt�k s�lyozott kombin�ci�j�val.
}

void VisualizeGPUSpecifications(cudaDeviceProp* prop)
{
	printf("GPU Specification:\n");
	printf(" - Compute Capability: %d.%d\n", prop->major, prop->minor);
	printf(" - Max threads per block: %d\n", prop->maxThreadsPerBlock);
	printf(" - Max shared memory size per block: %d\n", prop->sharedMemPerBlock);

}

int main()
{
	//Vars

	uint8_t* img;
	uint8_t* dev_img;
	
	uint8_t* result;
	uint8_t* dev_result;

	cudaEvent_t start, stop;

	cudaDeviceProp* prop;

	double gpu_time;

	int* deviceIndex;

	int width;
	int height;
	int channels;

	int size;

	// ************************ Inits ************************

	img = stbi_load("test.jpg", &width, &height, &channels, CHANNELS);
	size = width * height * CHANNELS;
	gpu_time = 0.0;

	//************************* Allocs *************************

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaMalloc((void**)&dev_img, size * sizeof(uint8_t));
	cudaMalloc((void**)&dev_result, width * height * sizeof(uint8_t));

	cudaError_t mallocresult = cudaGetLastError();
	if (mallocresult != cudaSuccess)
	{
		printf("Error allocating GPU memory!");
	}

	result = (uint8_t*)malloc(width * height * sizeof(uint8_t));
	mallocresult = cudaGetLastError();
	if (mallocresult != cudaSuccess)
	{
		printf("Error allocating CPU memory!");
	}

	deviceIndex = (int*)malloc(sizeof(int));
	prop = (cudaDeviceProp*)malloc(sizeof(cudaDeviceProp));
	cudaGetDevice(deviceIndex);
	cudaGetDeviceProperties(prop, *deviceIndex);

	//*************************MEMCPYS: CPU --> GPU*******************************

	cudaMemcpy(dev_img, img, size * sizeof(uint8_t), cudaMemcpyHostToDevice);
	cudaError_t status = cudaGetLastError();
	if (status != cudaSuccess)
	{
		printf("CPU --> GPU: copy error");
	}

	free(img);

	//************************Kernels************************************************

	VisualizeGPUSpecifications(prop);

	int dimGrid_x = (int)ceil((float)width / (float)TILE_SIZE);
	int dimGrid_y = (int)ceil((float)height / (float)TILE_SIZE);

	clock_t clk_start = clock();

	Convert2Grayscale << < dim3(dimGrid_x, dimGrid_y), dim3(TILE_SIZE, TILE_SIZE)>> > (dev_img, dev_result, width, height);
	cudaDeviceSynchronize();

	clock_t clk_end = clock();
	gpu_time = (double)(clk_end - clk_start) / (double)CLOCKS_PER_SEC;

	cudaFree(dev_img);

	//*************************MEMCPYS: GPU --> CPU*******************************

	cudaMemcpy(result, dev_result, height * width * sizeof(uint8_t), cudaMemcpyDeviceToHost);
	status = cudaGetLastError();
	if (status != cudaSuccess)
	{
		printf("GPU --> CPU: copy error");
	}

	cudaFree(dev_result);

	stbi_write_bmp("final.bmp", width, height, 1, result);

	status = cudaGetLastError();
	if (status == cudaSuccess)
	{
		printf("\nImage processed and saved to the default location of the project!");
		printf("\nTime ellapsed: %3.11f [sec]", gpu_time);
	}

	//Free result
	free(result);
}